## Specifications

The provided models have been trained on `human_dna_train_split.csv` and `human_dna_validation_split.csv`. Their prediction performance on `human_dna_test_split.csv`, the preprocessing procedure, and hyperparameters are stated in the report.

We applied the following model nomenclature: `{name of model}_humans.{pk,hdf5}`

### Model Overview
We made use of a variety of different model architectures:
- Gradient Boosting
- K-Nearest Neighbours
- Linear Support Vector Machine
- Logistic Regression
- MLP (Multilayer Perceptron)
- Random Forest
- SpliceAI400
- SpliceAI80
