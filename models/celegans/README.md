## Specifications

The provided models have been trained on `C_elegans_train_seq.csv`. Their prediction performance on `C_elegans_test_seq.csv`, the preprocessing procedure, and hyperparameters are stated in the report.

We applied the following model nomenclature: `{name of model}_celegans.{pk,hdf5}`


### Model Overview
We made use of a variety of different model architectures:
- Gradient Boosting
- K-Nearest Neighbours
- Linear Support Vector Machine
- Logistic Regression
- MLP (Multilayer Perceptron)
- Random Forest
- SpliceAI400
- SpliceAI80
