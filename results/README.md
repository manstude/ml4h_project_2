## Specifications

This repository contains the prediction file `results.pkl` of all models for the human, hidden_human and c.elegans test data, according to the instructions given by the TAs. Furthermore, the file `C_elegans_split.pkl` gives insights about the split of the `C_elegans_acc_seq.csv` dataset. The first column refers to the name of the split (train or test), the second column contains the values (sequences), and the third column holds the labels of each sample.

A short overview of the file structure `results.pkl` (one row for each model).
- Column 1: Model name
- Column 2: Training set name (either C. elegans or Human)
- Column 3: Trained model (if available)
- Column 4: AUROC on test set
- Column 5: AUPRC on test set
- Column 6: Predictions (decision function value or probabilities if available, otherwise labels) on the test set (ndarray)
- Column 7: Predictions (or labels if unavailable) for the hidden test set (leave empty for C. elegans rows)
- Column 8: ROC curve FPR values for Column 6 (ndarray)
- Column 9: ROC curve TPR values for Column 6 (ndarray)
- Column 10: PR curve Precision values for Column 6 (ndarray)
- Column 11: PR curve Recall values for Column 6 (ndarray)

